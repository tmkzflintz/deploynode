import requests
import json
import base64
import paho.mqtt.client as mqtt
import ssl
import time
import threading


def uploader(msg):
    m_decode = str(msg.payload.decode("utf-8", "ignore"))
    json_data = json.loads(m_decode)
    s = json_data['file'].encode("ascii")
    bin_data = base64.decodebytes(s)
    save_file(bin_data, json_data['version'])

    for ip in json_data['ip']:
        global client
        res = update_firmware("http://" + ip + "/update", json_data['version'])
        print(res)
        client.publish("/KISRA/DEV/SOLARCELL/UPLOAD/PI/RES", res)
        time.sleep(10)


def save_file(binary, name):
    f = open(name + ".bin", 'w+b')
    binary_format = bytearray(binary)
    f.write(binary_format)
    f.close()


def update_firmware(url, file_name):
    f = {'file': open(file_name + ".bin", 'rb')}
    try:
        r = requests.request('POST', url=url, files=f, timeout=60)
        if r.text == "OK":
            return "Complete"
        elif r.text == "FAIL":
            return "Fail"
        else:
            return "Fail"

    except requests.exceptions.Timeout:
        return "Timeout"
    except requests.RequestException:
        return "Fail"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe("/KISRA/DEV/SOLARCELL/UPLOAD/PI")
    client.subscribe("/KISRA/DEV/SOLARCELL/UPLOAD/PI/RES")


def on_message(client, userdata, msg):
    print("Receive " + msg.topic)

    topic = msg.topic

    if topic == "/KISRA/DEV/SOLARCELL/UPLOAD/PI":
        t1 = threading.Thread(target=uploader, args=(msg,))
        t1.setDaemon(True)
        t1.start()


username = "robotKs1"
password = "kisra.dev_iot"
port = 8883
broker = "128.199.222.164"

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
print("connecting to broker")
client.tls_set("ca.tcrt", tls_version=ssl.PROTOCOL_TLSv1_2)
client.tls_insecure_set(True)
client.username_pw_set(username, password)
client.connect(broker, port, 60)

try:
    client.loop_forever()
except KeyboardInterrupt:
    print('exit 0')
